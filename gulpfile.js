var config   = require("./package.json");
var gulp     = require("gulp");
var ejs      = require("gulp-ejs");
var babel    = require("gulp-babel");
var concat   = require("gulp-concat");
var wrapper  = require("gulp-wrapper");
var bower    = require("gulp-bower");
var compiler = require("gulp-closure-compiler");
var eslint   = require("gulp-eslint");

gulp.task("bower", function () {
    return bower();
});

gulp.task("header", function () {
    return gulp.src("templates/header.ejs")
        .pipe(ejs({ config : config }))
        .pipe(concat("header.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("babel-src", function () {
    return gulp.src([
            "src/database/main.js",
            "src/main.js"
        ], { base : "src" })
        .pipe(babel({
            whitelist : [
                "es6.blockScoping",
                "es6.classes",
                "es6.modules",
                "es6.templateLiterals",
                
                "strict"
            ],
            modules   : "amd",
            moduleIds : true,
            plugins   : ["object-assign"]
        }))
        .pipe(concat("babel-src.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("src", ["babel-src"], function () {
    return gulp.src([
            "dist/babel-src.js",
            "src/index.js"
        ])
        .pipe(concat("src.js"))
        .pipe(gulp.dest("dist"))
        .pipe(eslint({
            rules: {
                "global-strict"        : false,
                "no-underscore-dangle" : false,
                "eol-last"             : false,
                "no-trailing-spaces"   : false
            },
            globals : {
                "define"    : true,
                "requirejs" : true
            },
            envs : ["browser"]
        }))
        .pipe(eslint.format());
});

gulp.task("zeptojs", ["bower"], function () {
    return gulp
        .src("bower_components/zeptojs/index.js")
        .pipe(wrapper({
            header : 'define("zepto", ["exports", "module"], function (exports, module) { \n',
            footer : '\n module.exports = Zepto; });'
        }))
        .pipe(concat("zepto.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("alasql", function () {
    return gulp
        .src("node_modules/alasql/dist/alasql.js")
        .pipe(wrapper({
            header : 'define("alasql", ["exports", "module"], function (exports, module) { define = null; \n',
            footer : '\n });'
        }))
        .pipe(concat("alasql.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("simple_optimizations", ["alasql"], function () {
    return gulp
        .src([
            "dist/alasql.js"
        ])
        .pipe(concat("simple_optimizations.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("simple_optimizations.min", ["bower", "simple_optimizations"], function () {
    return gulp
        .src("dist/simple_optimizations.js")
        .pipe(compiler({
            compilerPath : "bower_components/closure-compiler/compiler.jar",
            fileName     : "simple_optimizations.min.js",
            compilerFlags : {
                language_in       : "ECMASCRIPT5",
                language_out      : "ECMASCRIPT5",
                compilation_level : "SIMPLE_OPTIMIZATIONS",
                warning_level     : "QUIET",
                // formatting        : "PRETTY_PRINT"
            }
        }))
        .pipe(gulp.dest("dist"));
});

gulp.task("advanced_optimizations", ["zeptojs", "src"], function () {
    return gulp
        .src([
            "node_modules/requirejs/require.js",
            "dist/zepto.js",
            "dist/src.js"
        ])
        .pipe(concat("advanced_optimizations.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("advanced_optimizations.min", ["bower", "advanced_optimizations"], function () {
    return gulp
        .src("dist/advanced_optimizations.js")
        .pipe(compiler({
            compilerPath : "bower_components/closure-compiler/compiler.jar",
            fileName     : "advanced_optimizations.min.js",
            compilerFlags : {
                language_in       : "ECMASCRIPT5",
                language_out      : "ECMASCRIPT5",
                compilation_level : "ADVANCED_OPTIMIZATIONS",
                warning_level     : "QUIET",
                externs           : [
                    "externs/requirejs.js"
                ],
                // formatting : "PRETTY_PRINT"
            }
        }))
        .pipe(gulp.dest("dist"));
});

gulp.task("user", ["header", "simple_optimizations", "advanced_optimizations"], function () {
    return gulp.src([
            "dist/header.js",
            "dist/advanced_optimizations.js",
            "dist/simple_optimizations.js"
        ])
        .pipe(concat(config.name + ".user.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("user.min", ["header", "simple_optimizations.min", "advanced_optimizations.min"], function () {
    return gulp.src([
            "dist/header.js",
            "dist/advanced_optimizations.min.js",
            "dist/simple_optimizations.min.js"
        ])
        .pipe(concat(config.name + ".min.user.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("default", ["user", "user.min"]);
