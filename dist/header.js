// ==UserScript==
// @name        asiandvdclub-sql-backup
// @description SQL backup of asiandvdclub data
// @license     MIT
// @namespace   http://asiandvdclub.org
// @include     /^https?:\/\/(?:www\.)?asiandvdclub\.org/
// @version     0.0.1
// @grant       none
// ==/UserScript==
