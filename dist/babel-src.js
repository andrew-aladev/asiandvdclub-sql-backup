define("database/main", ["exports", "module", "alasql"], function (exports, module, _alasql) {
    "use strict";

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

    function _interopRequire(obj) { return obj && obj.__esModule ? obj["default"] : obj; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    var _alasql2 = _interopRequire(_alasql);

    var Database = (function () {
        function Database(options) {
            _classCallCheck(this, Database);

            this.options = _extends({}, Database.defaults, options);

            this.init();
            this.migrate();
        }

        _createClass(Database, [{
            key: "init",
            value: function init() {
                _alasql2("CREATE localStorage DATABASE IF NOT EXISTS " + this.options.name);
                _alasql2("ATTACH localStorage DATABASE " + this.options.name + " AS " + this.options.name);
                _alasql2("USE " + this.options.name);
            }
        }, {
            key: "migrate",
            value: function migrate() {
                var tables = _alasql2("show tables");
                var tables_hash = {};
                if (Array.isArray(tables)) {
                    for (var index = 0; index < tables.length; index++) {
                        var table = tables[index];
                    }
                }
            }
        }]);

        return Database;
    })();

    Database.defaults = {
        name: "asiandvdclub"
    };

    module.exports = Database;
});
define("main", ["exports", "database/main"], function (exports, _databaseMain) {
  "use strict";

  function _interopRequire(obj) { return obj && obj.__esModule ? obj["default"] : obj; }

  var _Database = _interopRequire(_databaseMain);

  new _Database();
});