import alasql from "alasql";

class Database {
    static defaults;

    constructor (options) {
        this.options = Object.assign({}, Database.defaults, options);

        this.init();
        this.migrate();
    }

    init () {
        alasql(`CREATE localStorage DATABASE IF NOT EXISTS ${this.options.name}`);
        alasql(`ATTACH localStorage DATABASE ${this.options.name} AS ${this.options.name}`);
        alasql(`USE ${this.options.name}`);
    }

    migrate () {
        var tables      = alasql("show tables");
        var tables_hash = {};
        if (Array.isArray(tables)) {
            for (var index = 0; index < tables.length; index ++) {
                var table = tables[index];
            }
        }
    }
}
Database.defaults = {
    name : "asiandvdclub"
};

export default Database;
